import { Component, OnInit } from "@angular/core";
import { Propriedade } from "~/app/core/module";
import { DatabaseService } from "~/app/core/sqlite.service";

@Component ({
    selector: "app_crud",
    moduleId: module.id,
    templateUrl: "./sample.component.html",
    styleUrls: ["./sample.component.css"]
})



export class SampleComponent implements OnInit {

    propriedade: any;    
    db;
    constructor (private database : DatabaseService ){
        this.db = database.getdbConnection();
    }

    ngOnInit(): void {
        this.propriedade = new Propriedade();
    }

    cadPropriedade(nome: string) {
        this.db.then(db=>{
            db.execSQL("INSERT INTO PROPRIEDADE (NOME) VALUES (?);", [this.propriedade.nomePropriedade]);
            // console.log(this.propriedade.nomePropriedade);
            alert("inserido com sucesso");
            // this.propriedade.nomePropriedade = "";
        })
        console.log(this.propriedade.nomePropriedade);
        this.propriedade.nomePropriedade = "";
    }


    recPropriedades (){
        this.db
            .then(db => {
                db.all("SELECT * FROM PROPRIEDADE")
                    .then(rows => {
                        if (rows.length > 0) {
                            for (var row in rows){
                                this.propriedade.idPropriedade = rows[row][0]
                                this.propriedade.nomePropriedade = rows[row][1]
                                console.log("teste");
                            }
                        }
                    })
            })
    }
}