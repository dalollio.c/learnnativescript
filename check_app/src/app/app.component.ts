import { Component } from "@angular/core";
import { DatabaseService } from "./core/sqlite.service";

var Sqlite = require("nativescript-sqlite");
@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html"
})


export class AppComponent {
    
    public constructor(private database: DatabaseService)
    {
        this.database.getdbConnection()
        .then(db =>{
            db.execSQL("CREATE TABLE IF NOT EXISTS PROPRIEDADE (  ID INTEGER PRIMARY KEY AUTOINCREMENT, id_pro INTEGER, _produtor INTEGER, id_projeto INTEGER, NOME VARCHAR (20), nome_produtor VARCHAR (20), endereco VARCHAR (100), bairro VARCHAR (20),enviado VARCHAR(3), atividade VARCHAR(10),area_total VARCHAR(10), area_produtiva VARCHAR(10), num_funcionario INTEGER,municipio VARCHAR (40), uf VARCHAR (2),telefone VARCHAR (40), latitude VARCHAR (40), longitude VARCHAR (40));")
            .then(() => {}, error => { console.log("CREA TABLE ERROR", error);
            });
        })
    }
}
