import { Injectable } from "@angular/core";


var Sqlite = require("nativescript-sqlite");


@Injectable()
export class DatabaseService {
    
    public getdbConnection() {
        return new Sqlite('database');
    }

    public closedbConnection() {
        new Sqlite('database')
            .then((db) => {
                db.close();
            });
    }
}